dofile("gui.lua")

gui.debug = false

local s = gui.screen()
s:setBackgroundColor(colors.white)

local b = s:add(gui.button("X"))
b:setTextColor(colors.black)
b:setBackgroundColor(colors.red)
b:setPosition(-1, 1)
b:onClick(function(button, frame, event, x, y)
    frame:getScreen():stop()
	os.reboot()
end)

    local f = s:frame() -- includes add
    f:setPositions(3, 3, -3, -3)
    f:setTextColor(colors.black)
    f:setBackgroundColor(colors.blue)

    local t = gui.text("Open Gate");
    f:add(t)

    local t = gui.text("Close Gate");
    f:add(t)
    
    local menu = f:frame()
    menu:setPosition(-8, 11)
    menu:setSize(8, 10)
    menu.hide = true
    menu:setTextColor(colors.black)
    menu:setBackgroundColor(colors.lime)
    menu:add(gui.text("Close Gate"))

    local m = f:add(gui.button("Open Gate"))
    m:setTextColor(colors.black)
    m:setBackgroundColor(colors.gray)
    m:setPosition(-1, -1, "center")
    m:onClick(function(button, frame, event, x, y)
        while true do --Will loop forever
  rs.setOutput("left", true) --Set the redstone output to the specified side to true
  sleep(60*1) -- wait 60 seconds
  rs.setOutput("left", false) --Set it to false
  sleep(60*5) -- sleep for 5 minutes
end
		menu.hide = not menu.hide
        frame:getScreen():draw()
    end)

    local m = f:add(gui.button("Close Gate"))
    m:setTextColor(colors.black)
    m:setBackgroundColor(colors.gray)
    m:setPosition(-1, -1, "left")
    m:onClick(function(button, frame, event, x, y)
        while true do --Will loop forever
  rs.setOutput("left", false) --Set the redstone output to the specified side to true
  sleep(5) -- wait 5 seconds
  rs.setOutput("left", false) --Set it to false
  sleep(60*5) -- sleep for 5 minutes
end
		menu.hide = not menu.hide
        frame:getScreen():draw()
    end)

s:draw()
s:wait()
s:reset()
